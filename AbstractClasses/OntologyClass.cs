﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace OntologyClasses.AbstractClasses
{
    public abstract class OntologyClass
    {
        public bool PartialEquals(object obj, List<PropertyFilter> properties)
        {

            try
            {
                foreach (var prop in properties)
                {
                    var result = prop.CompareValues(this, obj);
                    if (!result)
                    {
                        return false;
                    }
                }    
            }
            catch (Exception)
            {
                return false;

            }
               
            
            return true;


        }
    }
}
