﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClasses.Interfaces
{
    public interface ILocalConfig
    {
        string IdLocalConfig { get; }
    }
}
