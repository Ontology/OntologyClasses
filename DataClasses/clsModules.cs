﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClasses.DataClasses
{
    public class clsModules
    {
        public clsOntologyItem Module_OntologyModule { get; private set; }

        public List<clsOntologyItem> Modules { get; private set; }

        private clsClasses objClasses = new clsClasses();
        private clsTypes objTypes = new clsTypes();

        public clsModules()
        {
            Modules = new List<clsOntologyItem>();
            Module_OntologyModule = new clsOntologyItem
            {
                GUID = "beaf70122f2a4b6783f8d99b9f018fbf",
                Name = "Sem-Manager",
                GUID_Parent = objClasses.OItem_Class_Module.GUID,
                Type = objTypes.ObjectType
            };

            Modules.Add(Module_OntologyModule);
        }
    }
}
