﻿using System;
using System.Data.SqlTypes;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace OntologyClasses.BaseClasses
{
    [Flags]
    public enum TestType
    {
        None = 0,
        Equal = 1,
        Different = 2,
        Contains = 4,
        Greater = 8,
        Smaller = 16,
        CaseSensitive = 32,
        Regex = 65
    }
    public class PropertyFilter
    {
        public string PropertyName { get; set; }
        public TestType TestTypeOfProperty { get; set; }
        public object FilterObject { get; set; }

        public bool CompareValues(object item1, object item2)
        {

            if (item2 != null)
            {
                FilterObject = item2;
            }
            try
            {
                var property = item1.GetType().GetProperty(PropertyName);
                var value1 = property.GetValue(item1);
                var value2 = property.GetValue(item2);
                if (value1.GetType() == value2.GetType())
                {
                    if (value1 == null || value2 == null)
                    {
                        if (TestTypeOfProperty.HasFlag(TestType.Equal))
                        {
                            return value1 == value2;
                        }
                        else if (TestTypeOfProperty.HasFlag(TestType.Different))
                        {
                            return value1 != value2;
                        }
                        else
                        {

                            return false;
                        }

                    }
                    else
                    {
                        if (value1 is string)
                        {
                            var valStr1 = value1.ToString();
                            var valStr2 = value2.ToString();


                            if (TestTypeOfProperty.HasFlag(TestType.Regex))
                            {
                                var regexValue = new Regex(valStr2);
                                if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                {
                                    return regexValue.Match(valStr1).Success;
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                {
                                    return !regexValue.Match(valStr1).Success;
                                }
                                else
                                {
                                    return false;
                                }


                            }
                            else
                            {
                                if (TestTypeOfProperty.HasFlag(TestType.Contains))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.CaseSensitive))
                                    {
                                        if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                        {
                                            return valStr1.Contains(valStr2);
                                        }
                                        else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                        {
                                            return !valStr1.Contains(valStr2);
                                        }
                                        else
                                        {
                                            return false;
                                        }

                                    }
                                    else
                                    {
                                        if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                        {
                                            return valStr1.ToLower().Contains(valStr2.ToLower());
                                        }
                                        else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                        {
                                            return !valStr1.ToLower().Contains(valStr2.ToLower());
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.CaseSensitive))
                                    {
                                        if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                        {
                                            return valStr1 == valStr2;
                                        }
                                        else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                        {
                                            return valStr1 != valStr2;
                                        }
                                        else
                                        {
                                            return false;
                                        }

                                    }
                                    else
                                    {
                                        if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                        {
                                            return valStr1.ToLower() == valStr2.ToLower();
                                        }
                                        else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                        {
                                            return valStr1.ToLower() != valStr2.ToLower();
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }

                                }
                            }


                        }
                        else
                        {
                            
                            if (value1 is bool)
                            {
                                bool valBool1 = (bool)value1;
                                bool valBool2 = (bool)value2;

                                if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                {


                                    return valBool1 == valBool2;

                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Different))
                                {
                                    return valBool1 != valBool2;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else if (value1 is int || value1 is long)
                            {
                                var valInt1 = (long)value1;
                                var valInt2 = (long)value2;

                                if (TestTypeOfProperty == TestType.Equal)
                                {
                                    return valInt1 == valInt2;

                                }
                                else if (TestTypeOfProperty == TestType.Different)
                                {
                                    return valInt1 != valInt2;
                                }
                            
                                if (TestTypeOfProperty.HasFlag(TestType.Greater))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valInt1 >= valInt2;
                                    }
                                    else
                                    {
                                        return valInt1 > valInt2;    
                                    }
                                
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Smaller))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valInt1 <= valInt2;
                                    }
                                    else
                                    {
                                        return valInt1 < valInt2;    
                                    }
                                
                                }
                                else
                                {
                                    return false;
                                }

                            }
                            else if (value1 is double)
                            {
                                var valDbl1 = (double)value1;
                                var valDbl2 = (double)value2;

                                if (TestTypeOfProperty == TestType.Equal)
                                {
                                    return valDbl1.Equals(valDbl2);

                                }
                                else if (TestTypeOfProperty == TestType.Different)
                                {
                                    return !valDbl1.Equals(valDbl2);
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Greater))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valDbl1 >= valDbl2;    
                                    }
                                    else
                                    {
                                        return valDbl1 > valDbl2;
                                    }
                                    
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Smaller))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valDbl1 <= valDbl2;
                                    }
                                    else
                                    {
                                        return valDbl1 < valDbl2;    
                                    }
                                    
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else if (value1 is DateTime)
                            {
                                var valDateTime1 = (DateTime)value1;
                                var valDateTime2 = (DateTime)value2;

                                if (TestTypeOfProperty == TestType.Equal)
                                {
                                    return valDateTime1 == valDateTime2;

                                }
                                else if (TestTypeOfProperty == TestType.Different)
                                {
                                    return valDateTime1 != valDateTime2;
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Greater))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valDateTime1 >= valDateTime2;
                                    
                                    }
                                    else
                                    {
                                        return valDateTime1 > valDateTime2;
                                    }
                                    
                                }
                                else if (TestTypeOfProperty.HasFlag(TestType.Smaller))
                                {
                                    if (TestTypeOfProperty.HasFlag(TestType.Equal))
                                    {
                                        return valDateTime1 <= valDateTime2;
                                    }
                                    else
                                    {
                                        return valDateTime1 < valDateTime2;    
                                    }
                                    
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                return false;
                            }
                                
                        }



                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }
    }
}
