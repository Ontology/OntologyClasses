﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.AbstractClasses;

namespace OntologyClasses.BaseClasses
{
    public class clsObjectRel : OntologyClass
    {

        private long? orderID;

        public string id
        {
            get
            {
                return ID_Object + ID_Other + ID_RelationType;
            }
        }
        public string ID_Object { get; set; }
        public string Name_Object { get; set; }
        public string ID_Parent_Object { get; set; }
        public string Name_Parent_Object { get; set; }
        public string ID_Other { get; set; }
        public string Name_Other { get; set; }
        public string ID_Parent_Other { get; set; }
        public string Name_Parent_Other { get; set; }
        public string ID_RelationType { get; set; }
        public string Name_RelationType { get; set; }
        public string Ontology { get; set; }
        public string ID_Direction { get; set; }
        public string Name_Direction { get; set; }
        public long? OrderID 
        {
            get { return orderID; }
            set 
            { 
                orderID = value;
            } 
        }
        public long OrderIDNotNull { get { return orderID ?? 0; } }

        public override bool Equals(object obj)
        {
            var item = (clsObjectRel) obj;
            return (ID_Object == item.ID_Object &&
                    Name_Object == item.Name_Object &&
                    ID_Parent_Object == item.ID_Parent_Object &&
                    Name_Parent_Object == item.Name_Parent_Object &&
                    ID_Other == item.ID_Other &&
                    Name_Other == item.Name_Other &&
                    ID_Parent_Other == item.ID_Parent_Other &&
                    Name_Parent_Other == item.Name_Parent_Other &&
                    ID_RelationType == item.ID_RelationType &&
                    Name_RelationType == item.Name_RelationType &&
                    Ontology == item.Ontology &&
                    ID_Direction == item.ID_Direction &&
                    Name_Direction == item.Name_Direction &&
                    orderID == item.orderID);
        }

        public clsObjectRel()
        {
            
        }

        public clsObjectRel(string ID_Object,
                             string Name_Object,
                             string ID_Parent_Object,
                             string Name_Parent_Object,
                             string ID_Other,
                             string Name_Other,
                             string ID_Parent_Other,
                             string Name_Parent_Other,
                             string ID_RelationType,
                             string Name_RelationType,
                             string Ontology,
                             string ID_Direction,
                             string Name_Direction,
                             long? OrderID)
        {
            this.ID_Object = ID_Object;
            this.Name_Object = Name_Object;
            this.ID_Parent_Object = ID_Parent_Object;
            this.Name_Parent_Object = Name_Parent_Object;
            this.ID_Other = ID_Other;
            this.Name_Other = Name_Other;
            this.ID_Parent_Other = ID_Parent_Other;
            this.Name_Parent_Other = Name_Parent_Other;
            this.ID_RelationType = ID_RelationType;
            this.Name_RelationType = Name_RelationType;
            this.Ontology = Ontology;
            this.ID_Direction = ID_Direction;
            this.Name_Direction = Name_Direction;
            this.OrderID = OrderID;
        }

        public clsObjectRel(string ID_Object ,
                            string ID_Parent_Object ,
                            string ID_Other ,
                            string ID_Parent_Other ,
                            string ID_RelationType ,
                            string Ontology ,
                            string ID_Direction ,
                            long OrderID)
        {
            this.ID_Object = ID_Object;
            this.ID_Parent_Object = ID_Parent_Object;
            this.ID_Other = ID_Other;
            this.ID_Parent_Other = ID_Parent_Other;
            this.ID_RelationType = ID_RelationType;
            this.Ontology = Ontology;
            this.ID_Direction = ID_Direction;
            this.OrderID = OrderID;
        }

        public clsObjectRel Clone()
        {
            return new clsObjectRel
            {
                ID_Direction = this.ID_Direction,
                ID_Object = this.ID_Object,
                ID_Other =  this.ID_Other,
                ID_Parent_Object = this.ID_Parent_Object,
                ID_Parent_Other = this.ID_Parent_Other,
                ID_RelationType = this.ID_RelationType,
                OrderID = this.OrderID,
                Name_Direction = this.Name_Direction,
                Name_Object = this.Name_Object,
                Name_Other = this.Name_Other,
                Name_Parent_Object = this.Name_Parent_Object,
                Name_Parent_Other = this.Name_Parent_Other,
                Name_RelationType = this.Name_RelationType,
                Ontology = this.Ontology
            };
        }
    }
}
