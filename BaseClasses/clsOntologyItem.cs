﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using OntologyClasses.AbstractClasses;

namespace OntologyClasses.BaseClasses
{
    public class clsOntologyItem : OntologyClass
    {

        private const int cint_LeftRight = 1;
        private const int cint_RightLeft = 2;
        public List<clsOntologyItem> OList_Rel { get; set; }

        public string GUID { get; set; }

        public string id
        {
            get { return GUID; }
        }
        public string ID_Item
        {
            get { return GUID; }
            set { GUID = value; }
        }
        public string ID_Parent
        {
            get { return GUID_Parent; }
            set { GUID_Parent = value; }
        }
        public string ID_Class
        {
            get { return GUID_Parent; }
            set { GUID_Parent = value; }
        }
        public string ID_DataType
        {
            get { return GUID_Parent; }
            set { GUID_Parent = value; }
        }
        public string GUID_Parent { get; set; }
        public string GUID_Related { get; set; }
        public string GUID_Relation { get; set; }
        public string Name { get; set; }
        public string Name_Item
        {
            get { return Name; }
            set { Name = value; }
        }
        public string Name_Parent { get; set; }
        public string Caption { get; set; }
        public string Additional1 { get; set; }
        public string Additional2 { get; set; }
        public string Type { get; set; }
        public string Filter { get; set; }
        public int? ImageID { get; set; }
        public int? Version { get; set; }
        public long? Level { get; set; }
        public bool? New_Item { get; set; }
        public bool? Deleted { get; set; }
        public bool? Mark { get; set; }
        public bool? ObjectReference { get; set; }
        public int? Direction { get; set; }
        public long? Min { get; set; }
        public long? Max1 { get; set; }
        public long? Max2 { get; set; }
        public long? Val_Long { get; set; }
        public bool? Val_Bool { get; set; }
        public DateTime? Val_Date { get; set; }
        public double? Val_Real { get; set; }
        public string Val_String { get; set; }

        public long? Count { get; set; }

        public int Direction_LeftRight { get { return cint_LeftRight;  } }
        public int Direction_RightLeft { get { return cint_RightLeft; } }

        public string NewGuid { get { return Guid.NewGuid().ToString().Replace("-",""); }}

        public void add_OItem(clsOntologyItem OItem)
        {
            if (OList_Rel == null)
                OList_Rel = new List<clsOntologyItem>();
            OList_Rel.Add(OItem);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == this.GetType())
            {
                var testItem = (clsOntologyItem) obj;
                if (this.OList_Rel == null || testItem.OList_Rel == null)
                {
                    var result = this.OList_Rel == testItem.OList_Rel;
                    if (!result)
                    {
                        return false;
                    }
                }
                else
                {
                    var equalRels = (from rel1 in this.OList_Rel
                        join rel2 in testItem.OList_Rel on rel1 equals rel2
                        select rel1).ToList();

                    var result = equalRels.Count == this.OList_Rel.Count;
                    if (!result)
                    {
                        return false;
                    }
                }

                return (this.GUID == testItem.GUID &&
                        this.ID_Item == testItem.ID_Item &&
                        this.ID_Parent == testItem.ID_Parent &&
                        this.ID_Class == testItem.ID_Class &&
                        this.ID_DataType == testItem.ID_DataType &&
                        this.GUID_Parent == testItem.GUID_Parent &&
                        this.GUID_Related == testItem.GUID_Related &&
                        this.GUID_Relation == testItem.GUID_Relation &&
                        this.Name == testItem.Name &&
                        this.Name_Item == testItem.Name_Item &&
                        this.Name_Parent == testItem.Name_Parent &&
                        this.Caption == testItem.Caption &&
                        this.Additional1 == testItem.Additional1 &&
                        this.Additional2 == testItem.Additional2 &&
                        this.Type == testItem.Type &&
                        this.Filter == testItem.Filter &&
                        this.ImageID == testItem.ImageID &&
                        this.Version == testItem.Version &&
                        this.Level == testItem.Level &&
                        this.New_Item == testItem.New_Item &&
                        this.Deleted == testItem.Deleted &&
                        this.Mark == testItem.Mark &&
                        this.ObjectReference == testItem.ObjectReference &&
                        this.Direction == testItem.Direction &&
                        this.Min == testItem.Min &&
                        this.Max1 == testItem.Max1 &&
                        this.Max2 == testItem.Max2 &&
                        this.Val_Long == testItem.Val_Long &&
                        this.Val_Bool == testItem.Val_Bool &&
                        this.Val_Date == testItem.Val_Date &&
                        this.Val_Real == testItem.Val_Real &&
                        this.Val_String == testItem.Val_String &&
                        this.Count == testItem.Count);

            }
        
            return false;
        }

        public clsOntologyItem()
        {
            
        }

        public clsOntologyItem Clone()
        {
            return new clsOntologyItem
            {
                GUID = this.GUID,
                OList_Rel =  this.OList_Rel,
                ID_Item = this.ID_Item,
                ID_Parent = this.ID_Parent,
                ID_Class = this.ID_Class,
                ID_DataType = this.ID_DataType,
                GUID_Parent = this.GUID_Parent,
                GUID_Related = this.GUID_Related,
                GUID_Relation = this.GUID_Relation,
                Name = this.Name,
                Name_Item = this.Name_Item,
                Name_Parent = this.Name_Parent,
                Caption = this.Caption,
                Additional1 = this.Additional1,
                Additional2 = this.Additional2,
                Type = this.Type,
                Filter = this.Filter,
                ImageID = this.ImageID,
                Version = this.Version,
                Level = this.Level,
                New_Item = this.New_Item,
                Deleted = this.Deleted,
                Mark = this.Mark,
                ObjectReference = this.ObjectReference,
                Direction = this.Direction,
                Min = this.Min,
                Max1 = this.Max1,
                Max2 = this.Max2,
                Val_Long = this.Val_Long,
                Val_Bool = this.Val_Bool,
                Val_Date = this.Val_Date,
                Val_Real = this.Val_Real,
                Val_String = this.Val_String,
                Count = this.Count,
            };
        }

        public clsOntologyItem(string GUID_Item, 
                               string Type)
        {
            this.GUID = GUID_Item;
            this.Type = Type;
        }
       

        public clsOntologyItem(string GUID_Item,
                               string Name_Item,
                               string Type)
        {
            this.GUID = GUID_Item;
            this.Name = Name_Item;
            this.Type = Type;
        }
        
        public clsOntologyItem(string GUID_Item,
                               string Name_Item,
                               string GUID_Item_Parent,
                               string Type)
        {
            this.GUID = GUID_Item;
            this.Name = Name_Item;
            this.GUID_Parent = GUID_Item_Parent;
            this.Type = Type;
        }
    
        public clsOntologyItem(string GUID_Item,
                               string Name_Item,
                               string GUID_Relation,
                               string GUID_Related,
                               string Type)
        {
            this.GUID = GUID_Item;
            this.Name = Name_Item;
            this.GUID_Relation = GUID_Relation;
            this.GUID_Related = GUID_Related;
            this.Type = Type;
        }
        

        public clsOntologyItem(string GUID_Item,
                               string GUID_Relation,
                               string GUID_Related,
                               long? Level,
                               string Type)
        {
            this.GUID = GUID_Item;
            this.GUID_Relation = GUID_Relation;
            this.GUID_Related = GUID_Related;
            this.Level = Level;
            this.Type = Type;
        }
        
        
    }
}
