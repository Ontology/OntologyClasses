﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OntologyClasses.BaseClasses
{
    public class StringFilter
    {
        public bool CaseSensitive { get; set; }
        public string Filter { get; set; }
        public bool IsMatch(string value)
        {
            RegexOk = null;
            if (Regex)
            {
                RegexOk = true;
                try
                {
                    var regEx = new Regex(Filter);
                    return Positive ? regEx.Match(value).Success : !regEx.Match(value).Success;
                }
                catch (Exception)
                {

                    RegexOk = false;
                    return false;
                }
                
            }
            else
            {
                if (CaseSensitive)
                {
                    return value == Filter;
                }
                else
                {

                    if (value != null && Filter != null)
                    {
                        return value.ToLower() == Filter.ToLower();
                    }
                    else if (value == null && Filter == null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public bool Regex { get; set; }
        public bool? RegexOk { get; set; }
        public bool Positive { get; set; }
    }
}
