﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.AbstractClasses;

namespace OntologyClasses.BaseClasses
{
    /// <summary>
    /// 
    /// </summary>
    public class clsAttribute : OntologyClass
    {
        private bool? val_Bit;
        public bool? Val_Bit
        {
            get { return val_Bit; }
            set
            {
                val_Bit = value;
                ChangeDate = DateTime.Now;
            }
        }

        private long? val_Long;
        public long? Val_Long {
            get { return val_Long; }
            set { val_Long = value; }
        }
        public double? Val_Double { get; set; }
        public DateTime? Val_Date { get; set; }
        public string Val_String { get; set; }
        public string Val_Name { get; set; }

        public string ID_Attribute { get; set; }
        public string ID_AttributeType { get; set; }
        public string ID_DataType { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ChangeDate { get; set; }
        
        public clsAttribute(string ID_Attribute,
                            string ID_AttributeType,
                            string ID_DataType, 
                            bool? Val_Bit,
                            long? Val_Long,
                            double? Val_Real,
                            DateTime? Val_Date,
                            string Val_String,
                            string Val_Name)
        {
            this.Val_Bit = Val_Bit;
            this.Val_Long = Val_Long;
            this.Val_Double = Val_Real;
            this.Val_Date = Val_Date;
            this.Val_String = Val_String;
            this.Val_Name = Val_Name;
            this.ID_Attribute = ID_Attribute;
            this.ID_AttributeType = ID_AttributeType;
            this.ID_DataType = ID_DataType;
            CreateDate = DateTime.Now;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == this.GetType())
            {
                var compareItem = (clsAttribute)obj;
                return (this.Val_Bit == compareItem.Val_Bit &&
                        this.Val_Long == compareItem.Val_Long &&
                        this.Val_Double == compareItem.Val_Double &&
                        this.Val_Date == compareItem.Val_Date &&
                        this.Val_String == compareItem.Val_String &&
                        this.Val_Name == compareItem.Val_Name &&
                        this.ID_Attribute == compareItem.ID_Attribute &&
                        this.ID_AttributeType == compareItem.ID_AttributeType &&
                        this.ID_DataType == ID_DataType);
            }
            else
            {
                return false;
            }
            
        }

        public clsAttribute()
        {
            CreateDate = DateTime.Now;
        }
    }
}
