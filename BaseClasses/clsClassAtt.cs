﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OntologyClasses.AbstractClasses;

namespace OntologyClasses.BaseClasses
{
    public class clsClassAtt: OntologyClass
    {
        public string id
        {
            get
            {
                return ID_Class + ID_AttributeType;
            }
        }
        public string ID_Class { get; set; }
        public string Name_Class { get; set; }
        public string ID_AttributeType { get; set; }
        public string Name_AttributeType { get; set; }
        public string ID_DataType { get; set; }
        public string Name_DataType { get; set; }
        public long? Min { get; set; }
        public long? Max { get; set; }

        public clsClassAtt()
        {
            
        }

        public override bool Equals(object obj)
        {
            var item = (clsClassAtt) obj;
            return (ID_Class == item.ID_Class &&
                    Name_Class == item.Name_Class &&
                    ID_AttributeType == item.ID_AttributeType &&
                    Name_AttributeType == item.Name_AttributeType &&
                    ID_DataType == item.ID_DataType &&
                    Name_DataType == item.Name_DataType &&
                    Max == item.Max &&
                    Min == item.Min);

        }

        
        

        public clsClassAtt(string ID_AttributeType, string ID_DataType, string ID_Class, long? Min, long? Max)
        {
            this.ID_Class = ID_Class;
            this.ID_AttributeType = ID_AttributeType;
            this.ID_DataType = ID_DataType;

            this.Min = Min;
            this.Max = Max;
        }
    }
}
