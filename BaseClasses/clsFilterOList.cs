﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClasses.BaseClasses
{
    public class clsFilterOList<TType>
    {
        public List<TType> BaseList { get; set; }
        public List<TType> FilteredList { get; set; }
        public List<TType> ObligatoryPositiveFilterList { get; set; }
        public List<TType> ConditionalPositiveFilterList { get; set; }
        public List<TType> ObligatoryNegativeFilterList { get; set; }
        public List<TType> ConditionalNegativeFilterList { get; set; }
        public List<StringFilter> ObligatoryPositiveStringFilterList { get; set; }
        public List<StringFilter> ConditionalPositiveStringFilterList { get; set; }
        public List<StringFilter> ObligatoryNegativeStringFilterList { get; set; }
        public List<StringFilter> ConditionalNegativeStringFilterList { get; set; }
        public List<string> ObligatoryNullPropertyFilterList { get; set; }
        public List<string> ConditionalNullPropertyFilterList { get; set; } 

        public void AddFilter(TType oItem, bool obligatory, bool positive = true)
        {
            if (obligatory)
            {
                if (positive)
                {
                    ObligatoryPositiveFilterList.Add(oItem);        
                }
                else
                {
                    ObligatoryNegativeFilterList.Add(oItem);        
                }
                
            }
            else
            {
                if (positive)
                {
                    ConditionalPositiveFilterList.Add(oItem);    
                }
                else
                {
                    ConditionalNegativeFilterList.Add(oItem);    
                }
                
            }
            
        }

        public void AddStringFilter(string filter, bool obligatory, bool isRegex = false, bool positive = true, bool caseSensitive = false)
        {
            if (obligatory)
            {
                if (positive)
                {
                    ObligatoryPositiveStringFilterList.Add(new StringFilter
                    {
                        CaseSensitive =  caseSensitive,
                        Filter = filter,
                        Positive =  positive,
                        Regex = isRegex
                    });    
                }
                else
                {
                    ObligatoryNegativeStringFilterList.Add(new StringFilter { 
                        CaseSensitive =  caseSensitive,
                        Filter = filter,
                        Positive =  positive,
                        Regex = isRegex
                    });    
                }
                
            }
            else
            {
                if (positive)
                {
                    ConditionalPositiveStringFilterList.Add(new StringFilter
                    {
                        CaseSensitive = caseSensitive,
                        Filter = filter,
                        Positive = positive,
                        Regex = isRegex
                    });
                }
                else
                {
                    ConditionalNegativeStringFilterList.Add(new StringFilter
                    {
                        CaseSensitive = caseSensitive,
                        Filter = filter,
                        Positive = positive,
                        Regex = isRegex
                    });
                }
            }
        }

        public void RemoveFilter(TType oItem)
        {
            ObligatoryPositiveFilterList.RemoveAll(item => item.Equals(oItem));
            ConditionalPositiveFilterList.RemoveAll(item => item.Equals(oItem));
            ObligatoryNegativeFilterList.RemoveAll(item => item.Equals(oItem));
            ConditionalNegativeFilterList.RemoveAll(item => item.Equals(oItem));
        }

        public void RemoveStringFilter(string filter)
        {
            ObligatoryPositiveStringFilterList.RemoveAll(item => item.Filter == filter);
            ObligatoryNegativeStringFilterList.RemoveAll(item => item.Filter == filter);
            ConditionalPositiveStringFilterList.RemoveAll(item => item.Filter == filter);
            ConditionalNegativeStringFilterList.RemoveAll(item => item.Filter == filter);
        }

        public void RemoveNullPropertyFilter(string filter)
        {
            ObligatoryNullPropertyFilterList.RemoveAll(item => item == filter);
            ConditionalNullPropertyFilterList.RemoveAll(item => item == filter);
        }

        public void FilterList()
        {
            if (typeof (TType) == typeof (clsOntologyItem))
            {
                
            }
        }

        public bool FilterOntologyItemList()
        {
            var itemList = BaseList.Cast<clsOntologyItem>();
            var obligatoryPositiveFilterList = ObligatoryPositiveFilterList.Cast<clsOntologyItem>();
            var conditionalPositiveFilterList = ConditionalPositiveFilterList.Cast<clsOntologyItem>();

            if (obligatoryPositiveFilterList.Any())
            {
                
            }
            return true;
        }

        public clsFilterOList(List<TType> oItem, bool caseSensitive = false)
        {
            BaseList = oItem;
            FilteredList = new List<TType>();

            ObligatoryPositiveFilterList = new List<TType>();
            ConditionalPositiveFilterList = new List<TType>();
            ObligatoryNegativeFilterList = new List<TType>();
            ConditionalNegativeFilterList = new List<TType>();
            ObligatoryPositiveStringFilterList = new List<StringFilter>();
            ConditionalPositiveStringFilterList = new List<StringFilter>();
            ObligatoryNegativeStringFilterList = new List<StringFilter>();
            ConditionalNegativeStringFilterList = new List<StringFilter>();
           // ObjligatoryNullPropertyFilterList = new List<string>();
            ConditionalNullPropertyFilterList = new List<string>();
        }
    }
}
